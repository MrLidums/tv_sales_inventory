package lv.sdafinalproject.tv_sales_inventory;

import lv.sdafinalproject.tv_sales_inventory.model.DataRecord;
import lv.sdafinalproject.tv_sales_inventory.services.CollectInventoryData;
import lv.sdafinalproject.tv_sales_inventory.services.CollectSoldData;
import lv.sdafinalproject.tv_sales_inventory.services.ReadFile;
import lv.sdafinalproject.tv_sales_inventory.services.ReadMSExcelFile;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import java.util.Properties;

@SpringBootApplication
public class TvSalesInventoryApplication {

    public static void main(String[] args) {

        SpringApplication.run(TvSalesInventoryApplication.class, args);

    }
}
