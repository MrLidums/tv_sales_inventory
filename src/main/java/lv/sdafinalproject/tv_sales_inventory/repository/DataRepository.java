package lv.sdafinalproject.tv_sales_inventory.repository;

import lv.sdafinalproject.tv_sales_inventory.model.DataRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DataRepository extends JpaRepository<DataRecord, String> {

    List<DataRecord> findByYear(Double year);
}
