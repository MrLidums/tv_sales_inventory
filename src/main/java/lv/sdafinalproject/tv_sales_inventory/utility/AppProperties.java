package lv.sdafinalproject.tv_sales_inventory.utility;

import lv.sdafinalproject.tv_sales_inventory.services.ReadFile;
import java.io.IOException;
import java.util.Properties;

public class AppProperties {

    private Properties adHourLimit;

    public AppProperties() {
        adHourLimit = new Properties();
        loadLimits();
    }

    public Properties getAdHourLimit() {
        return adHourLimit;
    }

    public void setAdHourLimit(Properties adHourLimit) {
        this.adHourLimit = adHourLimit;
    }

    private void loadLimits() {
//        ToDo: how to get proper rootPath?
//        String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath()
        String rootPath = "D:\\Java projects\\TV_Sales_Inventory\\target\\classes\\static\\";
        String appConfigPath = rootPath + "app.xml";

        try {
            ReadFile readFile = new ReadFile();
            adHourLimit.loadFromXML(readFile.fileInputStream(appConfigPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
