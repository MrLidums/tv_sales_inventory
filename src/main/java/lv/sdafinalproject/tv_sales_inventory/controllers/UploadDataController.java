package lv.sdafinalproject.tv_sales_inventory.controllers;

import lv.sdafinalproject.tv_sales_inventory.model.DataRecord;
import lv.sdafinalproject.tv_sales_inventory.repository.DataRepository;
import lv.sdafinalproject.tv_sales_inventory.services.CollectInventoryData;
import lv.sdafinalproject.tv_sales_inventory.services.CollectSoldData;
import lv.sdafinalproject.tv_sales_inventory.services.ReadFile;
import lv.sdafinalproject.tv_sales_inventory.services.ReadMSExcelFile;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Properties;

@RestController
public class UploadDataController {

    private static final String SOLD_INVENTORY_FILE =
            "C:\\Users\\MrLidums\\Documents\\TV_InventoryProject\\" +
                    "Spots_Inventory_for_Java_project_no_header.xlsx";

    private static Properties appProps = new Properties();

    @Autowired
    DataRepository dataRepository;

    @GetMapping("/test")
    public String test(){
        ReadFile readFile = new ReadFile();
        ReadMSExcelFile readMSExcelFile = new ReadMSExcelFile();
        CollectSoldData collectSoldData = new CollectSoldData();
        CollectInventoryData collectInventoryData = new CollectInventoryData();

        Sheet sheet = readMSExcelFile.getSheetData(readFile.fileInputStream(SOLD_INVENTORY_FILE));

        List<DataRecord> soldData = collectSoldData.collect(sheet);
        List<DataRecord> inventoryData = collectInventoryData.collect(sheet);

        dataRepository.saveAll(soldData);
        dataRepository.saveAll(inventoryData);

        return "All is cool";

    }

    @GetMapping("/download")
    public List<DataRecord> download(){
        return dataRepository.findByYear(2018d);
    }
}
