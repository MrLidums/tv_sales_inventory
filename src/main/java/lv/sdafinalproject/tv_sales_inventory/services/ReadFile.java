package lv.sdafinalproject.tv_sales_inventory.services;

import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

@Service
public class ReadFile {

    public FileInputStream fileInputStream(String path){

        try {
            File file = new File(path);
            return new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
