package lv.sdafinalproject.tv_sales_inventory.services;

import lv.sdafinalproject.tv_sales_inventory.model.DataFileColumn;
import lv.sdafinalproject.tv_sales_inventory.model.DataRecord;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static lv.sdafinalproject.tv_sales_inventory.model.DataFileColumn.*;

@Service
public class CollectSoldData {

    private String key = "";
    private DataRecord record = new DataRecord();
    private List<DataRecord> data = new ArrayList<>();

    public List<DataRecord> collect(Sheet sheet) {

        for (Row row : sheet) {
            //if there are no data go to next row
            if (rowWithNoData(row)) {
                continue;
            }
            if (isNewKey(row)) {
                //if there are already records, add current Record before work with new one
                addRecord();
                key = getKey(row);
                createRecord(row);
            } else {
                //keep calculating hours data and add results to month total
                record.setValue(newValue(row));
            }
        }
        data.add(record); //add last record
        return data;
    }

    private void addRecord() {
        if (!key.equals("")) {
            data.add(record);
        }
    }

    private void createRecord(Row row) {
        record = new DataRecord();
        //String types
        record.setId(key);
        record.setUniverse(getText(row, indexOfColumn(UNIVERSE)));
        record.setChannel(getText(row, indexOfColumn(CHANNEL)));
        record.setTargetGroup(getText(row, indexOfColumn(TARGET)));
        //numeric types
        record.setYear(getNumber(row, indexOfColumn(YEAR)));
        record.setMonthInYear(getNumber(row, indexOfColumn(MONTH_IN_YEAR)));
        //special fields
        specialDataFields(row);
    }

    private void specialDataFields(Row row) {
        //String types
        record.setUnit("Sold inventory");
        record.setStatus("Real");
        record.setComment("");
        //numeric types
        record.setValue(getNumber(row, indexOfColumn(TRP_VALUE)));
    }

    private String valueOfCell(Row row) {
        DataFormatter dataFormatter = new DataFormatter();
        return dataFormatter.formatCellValue(row.getCell(0));
    }

    private boolean isNewKey(Row row) {
        String newKey = getKey(row);
        return !key.equals(newKey);
    }

    private String getKey(Row row) {
        return new StringBuilder()
                .append(getText(row, indexOfColumn(YEAR)))
                .append(getText(row, indexOfColumn(MONTH_IN_YEAR)))
                .append(getText(row, indexOfColumn(CHANNEL)))
                .append(getText(row, indexOfColumn(TARGET)))
                .append(getText(row, indexOfColumn(UNIVERSE)))
                .append(specialKeyParts())
                .toString();
    }

    private String specialKeyParts() {
        return new StringBuilder()
                .append("Sold")
                .append("Real")
                .toString();
    }

    private boolean rowWithNoData(Row row) {

        boolean isEmpty = valueOfCell(row).equals("");
        boolean noData = (row.getCell(row.getLastCellNum() - 1).getCellType() == CellType.STRING);

        return isEmpty || noData;
    }

    private double getNumber(Row row, int cellIndex) {
        try {
            //get number from cell. in case number is stored as text - exception is returned
            return row.getCell(cellIndex).getNumericCellValue();
        } catch (IllegalStateException e) {
            //get number stored as text and convert it to number
            String value = row.getCell(cellIndex).getStringCellValue();
            return Double.parseDouble(value);
        }
    }

    private String getText(Row row, int cellIndex) {
        return row.getCell(cellIndex).getStringCellValue();
    }

    private int indexOfColumn(DataFileColumn enumName) {
        return DataFileColumn.valueOf(enumName.name()).ordinal();
    }

    private double newValue(Row row) {
        double currentValue = record.getValue();
        double nextValue = getNumber(row, indexOfColumn(TRP_VALUE));
        return currentValue + nextValue;
    }

}
