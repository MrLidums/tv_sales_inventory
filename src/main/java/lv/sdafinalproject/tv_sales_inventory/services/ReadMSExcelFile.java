package lv.sdafinalproject.tv_sales_inventory.services;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.IOException;

@Service
public class ReadMSExcelFile {

    public Sheet getSheetData(FileInputStream failStream) {

        try {
            Workbook workbook = new XSSFWorkbook(failStream);
            return workbook.getSheetAt(0);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }
}
