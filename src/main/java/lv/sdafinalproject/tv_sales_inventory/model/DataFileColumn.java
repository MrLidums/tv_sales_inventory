package lv.sdafinalproject.tv_sales_inventory.model;

/** This enum represents column positions in excel inventory data file **/
public enum DataFileColumn {
    UNIVERSE,
    TARGET,
    CHANNEL,
    DATE,
    HOUR,
    WEEK_IN_YEAR,
    MONTH_IN_YEAR,
    YEAR,
    TRP_VALUE,
    DURATION

}
