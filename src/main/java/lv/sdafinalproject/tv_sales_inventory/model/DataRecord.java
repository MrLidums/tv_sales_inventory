package lv.sdafinalproject.tv_sales_inventory.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "data")
public class DataRecord {

    @Id
    private String id; //is concatenated based on other fields

    private String universe;
    private double year;
    private double monthInYear;
    private String targetGroup;
    private String channel;
    private String unit;
    private String status;
    private double value;
    private String comment;


    public DataRecord(){
        this.value = 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUniverse() {
        return universe;
    }

    public void setUniverse(String universe) {
        this.universe = universe;
    }

    public double getYear() {
        return year;
    }

    public void setYear(double year) {
        this.year = year;
    }

    public double getMonthInYear() {
        return monthInYear;
    }

    public void setMonthInYear(double monthInYear) {
        this.monthInYear = monthInYear;
    }

    public String getTargetGroup() {
        return targetGroup;
    }

    public void setTargetGroup(String targetGroup) {
        this.targetGroup = targetGroup;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "DataRecord{" +
                "id='" + id + '\'' +
                ", universe='" + universe + '\'' +
                ", year=" + year +
                ", monthInYear=" + monthInYear +
                ", targetGroup='" + targetGroup + '\'' +
                ", channel='" + channel + '\'' +
                ", unit='" + unit + '\'' +
                ", status='" + status + '\'' +
                ", value=" + value +
                ", comment='" + comment + '\'' +
                '}';
    }


}
